﻿using Microsoft.EntityFrameworkCore;
using SaveTime.Core.DataModel.Business;
using SaveTime.Core.DataModel.Dictionary;
using SaveTime.Core.DataModel.Organization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Savetime.Core.DataAccess
{
    public class SaveTimeModel: DbContext
    {
        public SaveTimeModel(DbContextOptions options): base(options)
        {

        }

        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Record> Records { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Employer> Employers { get; set; }
    }
}
