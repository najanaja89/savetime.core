﻿using System.Collections.Generic;
using SaveTime.Core.Web.Admin.Models.Company;

namespace Savetime.Core.Web.Admin.Models.Company
{
    public class CompanyViewModel
    {
        //for Company/Index
        public IList<CompanyDetailsViewModel> Companies { get; set; } = new List<CompanyDetailsViewModel>();

        //for Company/Create
        public CreateCompanyViewModel CreateCompany { get; set; } = new CreateCompanyViewModel();
    }
}