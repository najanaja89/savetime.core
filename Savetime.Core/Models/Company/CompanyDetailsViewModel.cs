﻿using System.Collections.Generic;
using SaveTime.Web.Admin.Models.Branch;


namespace SaveTime.Core.Web.Admin.Models.Company
{
    public class CompanyDetailsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public List<BranchDetailsViewModel> Branches { get; set; }

    }
}