﻿namespace Savetime.Core.Web.Admin.Models.Company
{
    public class CreateCompanyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
    }
}