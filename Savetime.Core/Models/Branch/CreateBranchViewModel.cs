﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Savetime.Core.Web.Admin.Models.Company;
using SaveTime.Core.Web.Admin.Models.Company;

namespace SaveTime.Web.Admin.Models.Branch
{
    public class CreateBranchViewModel
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public DateTime StartWork { get; set; }
        public DateTime EndWork { get; set; }
        public int StepWork { get; set; }

        public IList<CompanyDetailsViewModel> Companies { get; set; } = new List<CompanyDetailsViewModel>();

        public string SelectedCompanyName { get; set; }
    }
}