﻿using AutoMapper;
using SaveTime.Core.DataModel;
using SaveTime.Core.DataModel.Organization;
using SaveTime.Web.Admin.Models.Branch;
using SaveTime.Web.Admin.Repo;
using SaveTime.Web.Admin.Repo.Impl;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Savetime.Core.Models;
using Savetime.Core.Web.Admin.Models.Company;
using SaveTime.Core.Web.Admin.Models.Company;

namespace SaveTime.Web.Admin.Controllers
{
    public class BranchController : BaseController
    {
        private readonly IRepository<Branch> _repositoryBranch;
        private readonly IRepository<Company> _repositoryCompany;
        readonly IMapper _mapper;

        public BranchController(IRepository<Branch> repositoryBranch, IRepository<Company> repositoryCompany)
        {
            _repositoryBranch = repositoryBranch;
            _repositoryCompany = repositoryCompany;

            var config = new MapperConfiguration(
                    c =>
                    {
                        c.CreateMap<Branch, BranchDetailsViewModel>();
                        c.CreateMap<Company, CompanyDetailsViewModel>();
                    }
                    );
            _mapper = config.CreateMapper();
        }
        public ActionResult Index()
        {
            var branches = _repositoryBranch.GetAll();
            var branch = new BranchViewModel();
            foreach (var branchItem in branches)
            {
                var bvm = _mapper.Map<BranchDetailsViewModel>(branchItem);
                branch.Branches.Add(bvm);
            }

            return View(branch);
        }

        [HttpGet]
        public ActionResult Create()
        {
            //var branch = new BranchViewModel();
            //var companies = _repositoryCompany.GetAll();
            //foreach (var company in companies)
            //{
            //    var cvm = _mapper.Map<CompanyDetailsViewModel>(company);
            //    branch.CreateBranch.Companies.Add(cvm);
            //}
            return View();
        }



        public JsonResult GetCompanies()
        {
            IEnumerable<Company> companies = _repositoryCompany.GetAll();
            var companyVM = new List<CompanyDetailsViewModel>();

            foreach (var com in companies)
            {
                var cvm = _mapper.Map<CompanyDetailsViewModel>(com);
                companyVM.Add(cvm);
            }
            return Json(companyVM);
        }

        [HttpPost]
        public JsonResult Create(CreateBranchViewModel createBranch)
        {

            var branch = new Branch()
            {
                Address = createBranch.Address,
                Email = createBranch.Email,
                StartWork = createBranch.StartWork,
                EndWork = createBranch.EndWork,
                Phone = createBranch.Phone,
                StepWork = createBranch.StepWork
            };
            _repositoryBranch.Create(branch);
            return Json(branch);
        }
    }
}