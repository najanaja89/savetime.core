﻿
using SaveTime.Core.DataModel.Organization;
using SaveTime.Web.Admin.Repo;
using SaveTime.Web.Admin.Repo.Impl;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace SaveTime.Web.Admin.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IRepository<Account> _repository;

        public AccountController(IRepository<Account> repository)
        {
            _repository = repository;
        }

        // GET: Account
        public ActionResult Index()
        {
            _repository.Create(new Account());

            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Account account)
        {
            _repository.Create(account);
            return View();
        }
    }
}