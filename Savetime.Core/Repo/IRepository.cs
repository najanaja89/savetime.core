﻿using SaveTime.Core.DataModel.Marker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveTime.Web.Admin.Repo
{
    public interface IRepository<T> where T : class, IEntity
    {
        void Create(T item);
        IEnumerable<T> GetAll();

        T GetById(int id);
        void Edit(T item);
        void Remove(T item);
    }
}
