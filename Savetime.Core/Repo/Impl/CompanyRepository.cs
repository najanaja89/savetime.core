﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Savetime.Core.DataAccess;
using SaveTime.Core.DataModel.Organization;

namespace SaveTime.Web.Admin.Repo.Impl
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly SaveTimeModel model;

        public CompanyRepository(SaveTimeModel m)
        {
            this.model = m;
        }
        public void Create(Company company)
        {
            model.Companies.Add(company);
            model.SaveChanges();
        }
    }
}