﻿using SaveTime.Core.DataModel.Marker;

namespace SaveTime.Core.DataModel.Dictionary
{
    public class Service : IEntity
    {
        public int Id { get; set; } // 1
        public string Title { get; set; } // Стрижка
    }
}
