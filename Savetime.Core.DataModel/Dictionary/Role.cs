﻿using SaveTime.Core.DataModel.Marker;

namespace SaveTime.Core.DataModel.Dictionary
{
    public class Role : IEntity
    {
        public int Id { get; set; } // 1, 2
        public string Title { get; set; } // Admin, Master

    }
}
