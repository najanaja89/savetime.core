﻿using SaveTime.Core.DataModel.Marker;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SaveTime.Core.DataModel.Organization
{
    public class Account : IEntity
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }

        public string Email { get; set; }

        public virtual IAccountOwner AccountOwner { get; set; }
    }
}
